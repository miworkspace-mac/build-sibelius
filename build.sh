#!/bin/bash -ex

# CONFIG
prefix="Sibelius"
suffix=""
munki_package_name="Sibelius8"
display_name="Sibelius"
url=`./finder.sh`

# download it (-L: follow redirects)
curl -L -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' "${url}"

## Mount disk image on temp space
mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`

cp "${mountpoint}/"*.pkg Sibelius.pkg

hdiutil detach "${mountpoint}"

## Unpack
/usr/sbin/pkgutil --expand "Sibelius.pkg" pkg

mv "pkg/"Sibelius* Application.pkg
mkdir build-root
(cd build-root; pax -rz -f ../Application.pkg/Payload)

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.app' -or -name '*.plugin' -or -name '*.prefPane' -or -name '*component' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo -m go-w -g admin -o root Sibelius.pkg "${key_files}" | /bin/bash > app.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist

plist=`pwd`/app.plist

#mv "build-root/Applications/"Sibelius* Sibelius.app

# Obtain version info
version=`/usr/libexec/PlistBuddy -c "print :CFBundleShortVersionString" build-root/Applications/*.app/Contents/Info.plist`

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.15.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" version "${version}"
defaults write "${plist}" unattended_install -bool TRUE


# Obtain display name from Izzy and add to plist
display_name=`/usr/local/bin/displaynametool "${munki_package_name}"`
defaults write "${plist}" display_name -string "${display_name}"

# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool "${munki_package_name}"`
defaults write "${plist}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}"


# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv Sibelius.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist
