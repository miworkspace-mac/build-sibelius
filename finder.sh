#!/bin/bash -x

NEWLOC=`curl -L "https://snootles.dsc.umich.edu/packages/sibelius" 2>/dev/null | /usr/local/bin/htmlq -a href a | grep .dmg | tail -1 | sed 's@\.\/@@'`

if [ "x${NEWLOC}" != "x" ]; then
	echo "https://snootles.dsc.umich.edu/packages/sibelius/${NEWLOC}"
fi
